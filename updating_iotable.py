#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  1 14:51:23 2019

@author: sarahnordahl
"""

import pandas as pd

co2_filepath = "waste/io_tables/impact_vectors/co2_impact.csv"
ch4_filepath = "waste/io_tables/impact_vectors/ch4_impact.csv"
n2o_filepath = "waste/io_tables/impact_vectors/n2o_impact.csv"
co_filepath = "waste/io_tables/impact_vectors/co_impact.csv"
nox_filepath = "waste/io_tables/impact_vectors/nox_impact.csv"
pm25_filepath = "waste/io_tables/impact_vectors/pm25_impact.csv"
so2_filepath = "waste/io_tables/impact_vectors/so2_impact.csv"
voc_filepath = "waste/io_tables/impact_vectors/voc_impact.csv"

io_filepath = "waste/io_tables/io_table_physical.csv"
io_data = pd.read_csv(io_filepath)  

filepath_list = [co2_filepath, ch4_filepath, n2o_filepath, co_filepath,
                 nox_filepath, pm25_filepath, so2_filepath, voc_filepath]

new_values_dict = {'co2':0, 'ch4':0,'n2o':0,'co':0,'nox':0,'pm25':0,'so2':0,'voc':0}

# upstream impacts dictionary is specific for particular parameter 
upstream_impacts = {}

guide_dict = {co2_filepath:'co2',
              ch4_filepath:'ch4',
              n2o_filepath:'n2o',
              co_filepath:'co',
              nox_filepath:'nox',
              pm25_filepath:'pm25',
              so2_filepath:'so2',
              voc_filepath:'voc'}



def UpdateImpactVectors(filepath_list, new_values_dict, parameter_name):
    ## Adds new parameter to all impact vector csv files 
    ## Args:
    #   filepath_list: list of filepath names of all impact vector csv files to be updated
    #   io_filepath: filepath name to physical units IO table
    #   new_values_dict: key = emission type, value = emission factor
    #   parameter_name (ex. organics_composting.wet_kg)
    for file in filepath_list:
        df = pd.read_csv(file)
        emission = guide_dict[file]
        value = new_values_dict[emission]
        df.loc[len(df)] = [parameter_name, value]
        df.to_csv(file, index = False)

def UpdateIOTable(io_filepath, parameter_name, upstream_impacts={}):
    ## Adds new parameter to io table csv
    # Args:
    # io_filepath: filepath to io physical units csv
    # parameter_name: name of new parameter (string)
    # upstream_impacts: dictionary with upstream uses in production of parameter product/process (key = upstream_paramater_name, value)
    # default for upstream_impacts is none (i.e. new row in IO tables is all zeroes)
    # being loose with terminology (upstream_impacts should also include any downstream impacts from given process/product)   
    df = pd.read_csv(io_filepath)
    df[parameter_name] = 0
    temp = list([parameter_name])
    temp2 = [0]*(len(df.columns)-1)
    temp.extend(temp2)
    df.loc[len(df)] = temp
    for key, value in upstream_impacts.items():
        df[key][df['products'] == parameter_name] = value
    df.to_csv(io_filepath, index = False)


def RemoveImpactVector(filepath_list, parameter_name):
    ### edit this so it also removes from IO tables !!!!!!!!!!!!!!!!!!!!!!!!
    ## Removes parameter and value from all impact vector csv files 
    for file in filepath_list:
        df = pd.read_csv(file)
        df = df[df['products'] != parameter_name]
        df.to_csv(file, index = False)
              
def ExtractIOInfo(parameter_name, io_df=io_data):
    # returns dataframe with upstream impacts of specified parameter
    # being loose with terminology (upstream_impacts should also include any downstream impacts from given process/product)   
    upstream_impacts = io_df[io_df['products'] == parameter_name].transpose()
    upstream_impacts = upstream_impacts[upstream_impacts.iloc[:,0] != 0]
    upstream_impacts = upstream_impacts.rename(columns=upstream_impacts.iloc[0])
    upstream_impacts = upstream_impacts.drop('products')
    return(upstream_impacts)



