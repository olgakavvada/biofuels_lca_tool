              #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 15:34:04 2019

@author: sarahnordahl
"""

import pandas as pd
import numpy as np
import os
from pint import UnitRegistry 
ureg = UnitRegistry()
import statistics as s
os.chdir("/Users/sarahnordahl/Desktop/biofuels_lca_tool")
#import parameters as P
import helper_functions as hf
from waste import Waste_functions as wf
from CreatePDF import PartialListPDF

co2_filepath = "waste/io_tables/impact_vectors/co2_impact.csv"
ch4_filepath = "waste/io_tables/impact_vectors/ch4_impact.csv"
n2o_filepath = "waste/io_tables/impact_vectors/n2o_impact.csv"
io_table_physicalunits_path = "waste/io_tables/io_table_physical.csv"

io_data = pd.read_csv(io_table_physicalunits_path).fillna(0)

ch4_energy = 0.656*55.5*(1/3.6) #kWh/m3 #methane density*heat val*conversion 
biogas_density_kg_m3 = 1.15

feedstock_types = ["food_waste", "yard_trimmings", "mixed_MSW","manure","ag_residues"]
selectivity_waste = ["bau_landfill","bau_compost", "bau_combo","ad_dry", "ad_wet","gasification"]
#bau_combo = landfilling mixed MSW, burning agricultural residues and composting all other organic waste
selectivity_ad_finaldest = ["landfill", "compost","land_app"]
selectivity_ad_enrecovery = ["electricity", "RNG"]

biogas_yield = pd.read_excel("waste/waste_descriptions.xlsx", sheet_name="biogas_yield", index_col = 0) #ft3 methane/BDT 
temp_biogas = {'mixed_food':'food_waste', 'green':'yard_trimmings', 'msw_food':'mixed_MSW', 'manure_dairy':'manure','woody':'ag_residues'}
syngas_yield = pd.read_excel("waste/waste_descriptions.xlsx", sheet_name="syngas_yield")
syngas_yield = dict(zip(syngas_yield.syngas_category,syngas_yield.gasification_energy)) # kWh/BDT 
## reallllly need syngas yields for different feedstocks (sarah smith just has paper, rice_husks, wood)
## for right now, just using smallest value 
biochar_yield = dict(pd.read_excel("waste/waste_descriptions.xlsx", sheet_name="biochar_yield", index_col = 0)) #percent
biochar_yield = dict(biochar_yield["biochar_yield"])
moisture_content = dict(pd.read_excel("waste/waste_descriptions.xlsx", sheet_name="mc", index_col = 0))
## ^^need these categories to correspond with feedstock_types (for all yield dicts/dfs)
# example:
moisture_content_values = [0.78, 0.4, 0.6, 0.8, 0.25]
moisture_content = dict(zip(feedstock_types, moisture_content_values))


## inputs
input_total_tonnage = 100 # wet tonnes 
input_feedstock_dist = [.20,.30,.30,.10,.10]
ad_dest = "compost"
ad_enrec = "electricity"
year = 2020
##

total_tonnage = input_total_tonnage
feedstock_dist = input_feedstock_dist

emission_type = 'ghg'

def WasteImpactModel(total_tonnage, feedstock_dist, ad_dest, ad_enrec, year, emission_type, pdf_filename = 'sample.pdf', credits=False):
    # Returns a dataframe of of all GHG emissions in the form of kg CO2e per tonne of waste  or 
    # water impacts in the form of Liters per tonne of waste per process
    #
    # Args:
    #  SP_params: process specific parameters dictionary
    #  model: string refering to whether the GHG model or the water model is run. Options are:
    #  'buttonGHG', 'buttonConsWater', 'buttonWithWater' for the GHG model, the water consumption model and
    #  the withdrawal model respectively
    
    # Returns:
    #  The net GHG emissions (kg CO2e) for the product life cycle by sector for model = 'buttonGHG'
    #  The net consumption water impacts (liters) for the product life cycle by sector for model = 'buttonConsWater'
    #  The net withdrawal water impacts (liters) for the product life cycle by sector for model = 'buttonWithWater'
    
    tonnage = dict(zip(feedstock_types, [i * total_tonnage for i in feedstock_dist]))
    
    #functional_unit = "kg per ton accepted"
    
    processes = ["Petroleum Products", "Diesel", "Other Electricity","Natural Gas", "Transportation", "Landfill", 
                 "Compost Application","Sequestration", "Biochar Application", "Chemicals", "Organics Composting",
                 "Agricultural Residue Burning", "Digestate Application", "Fertilizer Use", "CHP", "Gasification", 
                 "Facility Flare", "Biofilter Release", "Methane Loss", "Bladder", "Liquid Digestate Treatment", "Other"]
    if(year == 2020):
        processes.append("NGCC Electricity")
    else:
        processes.append("Electricity Mix")
        
    new_data = np.zeros([len(processes),len(selectivity_waste)])
    m = pd.DataFrame(new_data, columns=selectivity_waste, index=processes)
    
    
    for scenario in selectivity_waste:
        
        #scenario = "ad_dry"
        
        ## vector set up
        y = {}
        
        ## PROCESS CALCULATIONS
        
                    ## not included: Biofilter and Flare 
        
        # Transportation
        ave_distance = 35 #km
        if(scenario == "bau_combo"):
            y["flatbedtruck.mt_km"] = ave_distance*(total_tonnage - 0.5*tonnage["ag_residues"])
        else:
            y["flatbedtruck.mt_km"] = ave_distance*total_tonnage
        # Daily Electricity Consumption
        if(year == 2020):
            electricity = "electricity.NGCC.kWh"
        else:
            electricity = "Electricity_GridMix_2050.kWh"
        kdry = 3773/276     #ZWEDC number
        kwet = 1050.28/50   #Jay has source 
        rgas = 0.22         #22% of electricity generated is consumed (Jay has source)
                            # Assume no significant elec consumed for landfilling, composting, burning
        if(scenario == "ad_dry"):
            y[electricity] = total_tonnage*kdry
        elif(scenario == "ad_wet"):
            y[electricity] = total_tonnage*kwet
        else:
            y[electricity] = 0 # Will subtract off elec consumed during gasification when elec_gen is calculated (as well elec cons for gas upgrading when RNG)
        # BAU
        # Assumption: bau_combo means all mixed MSW is landfilled, half of all ag residues are burned, the rest is composted 
        # Landfilling
        if(scenario == 'bau_landfill'):
            y["landfill_foodwaste.wet_kg"] = tonnage['food_waste']
            y["landfill_yard_trimmings.wet_kg"] = tonnage['yard_trimmings']
            y["landfill_mixedMSW.wet_kg"] = tonnage['mixed_MSW']
            #y[] = tonnage['manure']
            #y[] = tonnage['ag_residues']
        elif(scenario == "bau_combo"):
            y["landfill_mixedMSW.wet_kg"] = tonnage['mixed_MSW']
        # Composting
        if(scenario == 'bau_compost'):
            y["organics_composting_wet.kg"] = total_tonnage*1000 #kg
            #y["outdoor_compost.wet_kg"] 
            y["compost_application.kg"] = total_tonnage*0.67*1000 #33% mass reduction (Ref: Measured data from Chelsea).  
            y["urea.kg"] = -total_tonnage*.185*0.67*1000
        elif(scenario == 'bau_combo'):
            compost_tonnage = total_tonnage - tonnage['mixed_MSW'] - 0.5*tonnage['ag_residues']
            y["organics_composting_wet.kg"] = compost_tonnage*1000 #kg
            #y["outdoor_compost.wet_kg"] 
            y["compost_application.kg"] = compost_tonnage*0.67*1000 #33% mass reduction (Ref: Measured data from Chelsea).  
            y["urea.kg"] = -compost_tonnage*.185*0.67*1000
        # Burning
        if(scenario == 'bau_combo'):
            y["Ag_residues_burning.kg"] = 0.5*tonnage['ag_residues']*1000 #kg 
        # FLARE, CHP, NGCC  ---- biogas (ad) & syngas (gasification)
        if(scenario in ["ad_dry", "ad_wet","gasification"]):
             # mc food_waste = food, ag_residues = field residue or crop residue or orchard residue?? (very diff #s), 
             # mixed_MSW = , yard_trimmings = leaves and grass, manure = 
             # assume flaring of 20% for all facility types 
             dry_tonnage = {k: tonnage[k]*(1-moisture_content[k]) for k in tonnage} 
             flaring = 0.2
             ft3_to_m3 = 1*ureg.cubic_feet
             ft3_to_m3.ito(ureg.meter**3) ## m3/ft3
             pump_kWh = (43826/3600)*(10**(-6)) #kWh consumed / kWh of gas pumped  
             # 1 kWh = 3412.14163312 BTU; 43,826 Btu/MMBtu for 3600 miles from CA GREET model  (to get pump.kWh)
             if(scenario == 'ad_dry'):
                 biogas_yield_addry = biogas_yield['ch4_ad_dry']
                 biogas_yield_addry = dict((value, biogas_yield_addry[key]) for (key, value) in temp_biogas.items())
                 biomethane = {k: dry_tonnage[k]*biogas_yield_addry[k] for k in dry_tonnage} 
                 biomethane.update((x, y*ft3_to_m3.magnitude) for (x,y) in biomethane.items())#m3 methane 
                 biomethane_kwh = ch4_energy * (1-flaring) * 0.4 # electrical efficiency should be 40% for biogas CHP (sarah smith)
                 y['biogas_flare.m3'] =  sum(biomethane.values())*2 * flaring #m3
                 if(ad_enrec == 'electricity'):
                     y['biogas_CHP.m3'] =  sum(biomethane.values())*2 * (1-flaring) #m3
                     y[electricity] += (-1)*biomethane_kwh*sum(biomethane.values()) #kWh
                 else:
                     y[electricity] += sum(biomethane.values())*pump_kWh*ch4_energy
                     y["naturalgas_combust.MJ", "y"] = (-1)*hf.FuelConvertMJ(sum(biomethane.values())*1000,'naturalgas','liter') #MJ 
             elif(scenario == 'ad_wet'):
                 biogas_yield_adwet = biogas_yield['ch4_ad_wet']
                 biogas_yield_adwet = dict((value, biogas_yield_adwet[key]) for (key, value) in temp_biogas.items())
                 biomethane = {k: dry_tonnage[k]*biogas_yield_adwet[k] for k in dry_tonnage} 
                 biomethane.update((x, y*ft3_to_m3.magnitude) for (x,y) in biomethane.items())#m3 methane 
                 biomethane_kwh = ch4_energy * (1-flaring) * 0.4 # electrical efficiency should be 40% for biogas CHP (sarah smith)
                 y['biogas_flare.m3'] = sum(biomethane.values())*2 * flaring #m3
                 if(ad_enrec == 'electricity'):
                     y['biogas_CHP.m3'] =  sum(biomethane.values())*2 * (1-flaring) #m3
                     y[electricity] += (-1)*biomethane_kwh*sum(biomethane.values()) #kWh
                 else:
                     y[electricity] += sum(biomethane.values())*pump_kWh*ch4_energy
                     y["naturalgas_combust.MJ", "y"] = (-1)*hf.FuelConvertMJ(sum(biomethane.values())*1000,'naturalgas','liter') #MJ 
             else: 
                 #syngas = {k: dry_tonnage[k]*syngas_yield[k] for k in dry_tonnage} #kWh
                 syngas = sum(dry_tonnage.values())*np.mean(list(syngas_yield.values()))
                 y['Gasification.kWh'] = syngas
                 y[electricity] += (-1)*(syngas*0.25)*(1-rgas) #25% engine efficiency #20% of electricity generated is consumed at facility 
        #DIGESTATE APPLICATION AND UREA FERTIlIZER CREDIT =====
        #Add digestate application to Y
        #When whole digestate is dewatered, 20% of the mass is removed in the liquor fraction, leaving a dewatered cake of approximately 80%. (Ref: Enhancement and treatment of digestates from anaerobic digestion)
        #Add fertilizer credit
        #assumes N:N compost:urea ratio of 8.5 : 46 (Ref: https://catalog.extension.oregonstate.edu/sites/catalog/files/project/pdf/pnw508_0.pdf)
        #digestate yields from Sarah Smith: 70% for dry AD, 34% for wet AD (remainder is liquid digestate), 34% for dairy and wwtp 
        if(scenario in ["ad_dry", "ad_wet"]):
            if(scenario == 'ad_dry'):
                if(ad_dest == 'land_app'):
                    y["biosolids_land_application_dry.kg"] = total_tonnage*0.7*1000 #kg
                    y["urea.kg"] = -total_tonnage*0.7*1000*0.185 #kg
                elif(ad_dest == 'compost'):
                    y['outdoor_compost.wet_kg'] = total_tonnage*0.7*1000 #kg 
                    y["compost_application.kg"] =  total_tonnage*0.7*1000*0.67 #33% mass reduction (Ref: Measured data from Chelsea).  
                    y["urea.kg"] = (-1)*total_tonnage*0.7*1000*.185*0.67
                    ###
                else: #landfill
                    #y["landfill_foodwaste.wet_kg"] = tonnage['food_waste']*0.7*1000
                    #y["landfill_yard_trimmings.wet_kg"] = (tonnage['yard_trimmings']+tonnage['manure']+tonnage['ag_residues'])*0.7*1000
                    #y["landfill_mixedMSW.wet_kg"]= tonnage['mixed_MSW']*0.7*1000
                    ## What is landfilling manure and ag residues most like???? 
                    ## for now, assume both can go into yard trimmings 
                    ##  OR ALTERNATIVELY
                    y['landfill_mixedorganics.wet_kg'] = total_tonnage*0.7*1000
            else:
                ## asssume land application for wet AD 
                y["biosolids_land_application_dry.kg"] = total_tonnage*0.34*1000 #kg
                y["urea.kg"] = -total_tonnage*0.34*1000*.185 
                liq_dig_density = 1 # m3/tonne ??? # FIND BETTER #/SOURCE 
                y["Liquid_Digestate_Treatment.m3"] = total_tonnage*(1-0.34)*liq_dig_density
        # BIOCHAR APPLICATION
        if(scenario == "gasification"):
            # biochar yield categories that I have right now don't line up at all with feedstocks so for the meantime, just using an average
            #biochar = {k: tonnage[k]*biochar_yield[k]["gasification"] for k in tonnage} 
            biochar = total_tonnage*s.mean(biochar_yield.values())
            y['biochar_ag_application.kg'] = biochar*1000 #kg 
        
        
        
        ### create PDF report showing relevant emission factors 
        ##
        ##PartialListPDF(y, file_name= pdf_filename, io_data = io_data)
        
        ########
        normalization_factor = total_tonnage        
        y.update((x, y/normalization_factor) for x, y in y.items())        
        
        if emission_type == 'ghg':
            
            y1 = y.copy() 
            for key, value in y1.items():
                if value > 0:
                    y1[key] = 0
                else:
                    y1[key] = abs(value)
            for key, value in y.items():
                if value < 0:
                    y[key] = 0

            
            results_kg_co2e = wf.TotalGHGEmissions(io_data, y, time_horizon = 100)
            results_kg_co2e_credits = wf.TotalGHGEmissions(io_data, y1, time_horizon = 100)
            results_kg_co2e['ghg_results_kg'] = results_kg_co2e['ghg_results_kg'] - results_kg_co2e_credits['ghg_results_kg']
        
            ipcc_ch4 = 28 ##for time_horizon = 100 yr

            # CNG LOSS
            if(scenario in ["ad_dry", "ad_wet"] and ad_enrec == "RNG"):
                k_loss = 0.02 #assume 2% leakage 
                cng_loss = sum(biomethane.values())*((1/(1-k_loss))-1)*biogas_density_kg_m3*ipcc_ch4*(1/normalization_factor)
                results_kg_co2e.loc[len(results_kg_co2e)] = [cng_loss, "CNG Loss"]
            else:
                results_kg_co2e.loc[len(results_kg_co2e)] = [0, "CNG Loss"]
            
            results_kg_co2e_dict = results_kg_co2e.set_index('products')['ghg_results_kg'].to_dict()
        
        elif emission_type in ['co','nox','pm25','so2','voc']:
            results_kg_co2e = wf.TotalEmissions(io_data, y, emission_type, time_horizon = 100)
            results_kg_co2e_dict = results_kg_co2e.set_index('products')[(emission_type+'_results_kg')].to_dict()
        else:
            ## separated out water for right now because I think y-vector calculations might change?
            results_kg_co2e = wf.TotalEmissions(io_data, y, emission_type, time_horizon = 100)
            results_kg_co2e_dict = results_kg_co2e.set_index('products')[(emission_type+'_results_kg')].to_dict()

        wf.AggregateResults(m, results_kg_co2e_dict, scenario, year)
        
    aggregated_data_avg = m[selectivity_waste].T
    aggregated_data_avg_plot = aggregated_data_avg[list(reversed(aggregated_data_avg.columns.values))]

    return aggregated_data_avg_plot



