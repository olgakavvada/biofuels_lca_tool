{
    "Enzymatic_Hydrolysis_and_Fermentation": {
        "cooling_water": 221.3673602323961,
        "steam_low": 0.6949142987352662,
        "cellulase_amount": 0.020409790954918062,
        "electricity": 5.58583767236303,
        "csl.kg": 0.21541874620220217,
        "inoculum.kg": 0.03146179387828483,
        "dap.kg": 0.027280528065570832
    },
    "Lignin_Utilization": {
        "electricity": 2.1497388149676033,
        "cooling_water25": 1251.5884046409103,
        "ng_input_stream_MJ": 1.1768048779028362
    },
    "Recovery_and_Separation": {
        "electricity": 0.1328868021331398,
        "cooling_water": 1528.8645876907203,
        "steam_low": 9.598465852377139,
        "steam_high": 20.68916314009472
    },
    "Wastewater_Treatment": {
        "electricity": 0.40782508139973916,
        "caoh.kg": 0.007503982937913542,
        "steam_low": 0.7685708181283721,
        "cip2.kg": 0.06003186350330834
    },
    "Credits": {
        "steam_generated": 55.90035059328365,
        "electricity_generated": 9.330165888238975
    },
    "Hydrogeneration_and_Oligomerization": {
        "Cyclohexane": 0.010441147976743339,
        "electricity": 0.009934306987752495,
        "PdAC_catalyst.kg": 0.0003264422072511025,
        "h2.kg": 0.018143095838274007,
        "Trifluoroacetic": 0.018143095838274007,
        "chilled_water": 22.728256806761543,
        "steam_low": 0.15493071843530587,
        "cooling_water": 1.823817471299871,
        "cooling_water25": 0.6206513968825309
    },
    "IL_Pretreatment": {
        "electricity": 0.09869804948554607,
        "steam_low": 0.9182525383975112,
        "ionicLiquid_amount": 0.014056105267934048,
        "acid": "h2so4",
        "acid.kg": 5.534970963578582
    },
    "Direct_Water": {
        "water_direct_withdrawal": 32.24,
        "water_direct_consumption": 24.55
    },
    "Feedstock_Handling_and_Preparation": {
        "electricity": 0.36682436084683206
    },
    "Feedstock_Supply_Logistics": {
        "feedstock.kg": 11.34583462150128
    }
}
