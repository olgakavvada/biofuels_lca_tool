#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 24 09:15:13 2019

@author: sarahnordahl
"""
import pandas as pd
import numpy as np
import math
import random
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Table, TableStyle, Paragraph
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle, TA_CENTER
#import Waste_functions as wf
import parameters as pp
#import updating_iotable as io
io_data = pd.read_csv(pp.io_table_physicalunits_path).fillna(0)

file_name = "sample.pdf"
y = {'atrazine.kg':1, 'glucose.kg':1,'electricity.NGCC.kWh':1,'sorghum.kg':1}
typefuel = 'Limonene'
feedstock = 'sorghum'
model = 'buttonGHG'
fuel = 'jet_fuel'

def ExtractIOInfo(parameter_name, io_df=io_data):
    # returns dataframe with upstream impacts of specified parameter
    # being loose with terminology (upstream_impacts should also include any downstream impacts from given process/product)   
    upstream_impacts = io_df[io_df['products'] == parameter_name].transpose()
    upstream_impacts = upstream_impacts[upstream_impacts.iloc[:,0] != 0]
    upstream_impacts = upstream_impacts.rename(columns=upstream_impacts.iloc[0])
    upstream_impacts = upstream_impacts.drop('products')
    return(upstream_impacts)


def PartialListPDF(y,feedstock, model, fuel, typefuel, file_name, io_data=io_data):  
     ## creates PDF file which lists GHG emission factors actually used in a specific LCA model run    
     ## ARGs:
     #  y = complete y vector from LCA model (actual values don't matter, as long as relevant impacts don't have zeroes)
     #      * y must be a dictionary! key (product): value (r value)
     #  file_name = name that the PDF file should save to    
     
    y = dict((k, v) for k, v in y.items() if v != 0)    
    
    selection = pd.DataFrame(y.keys())
    selection.columns = ['products']
    styleSheet = getSampleStyleSheet()
    style1 = ParagraphStyle(name= 'Custom',
                           fontName='Helvetica',
                           fontSize=10,
                           alignment = TA_CENTER,
                           parent = styleSheet['BodyText'])
    
    filepath_list = [pp.co2_filepath, pp.ch4_filepath, pp.n2o_filepath]
    #filepath_list = [wf.co2_filepath, wf.ch4_filepath, wf.n2o_filepath]
    
    ghg_emissions = [Paragraph('CO<sub>2</sub>',style1),Paragraph('CH<sub>4</sub>',style1),Paragraph('N<sub>2</sub>O',style1)]
    style = ParagraphStyle(name= 'Custom',
                           fontName='Helvetica',
                           fontSize=10,
                           parent = styleSheet['BodyText'])
    
    def ExtractImpacts(selection): 
        style = ParagraphStyle(name= 'Custom',
                          fontName='Helvetica',
                          fontSize=10,
                          alignment= TA_CENTER,
                          parent = styleSheet['BodyText'])
        for filepath, emission in zip(filepath_list, ghg_emissions):
            temp = pd.read_csv(filepath)
            temp = temp.loc[temp['products'].isin(selection['products'])]
            temp2 = [format(x, '.2e') for x in temp['r']]
            temp3 = []
            for value in temp2:
                number, exponent = value.split('e')
                if exponent[1] == "0":
                    exponent = exponent[0]+exponent[-1]
                text = number + " • 10<sup>"+exponent+"</sup>"
                temp3.append(Paragraph(text,style))
            selection[emission] = temp3        
        products = []
        units = []
        for i in range(len(selection['products'])):
            product_t = selection['products'][i]
            product_t = product_t.split('.')
            unit = product_t[-1]
            product = ""
            for k in range(len(product_t)-1):
                if len(product) == 0:
                    product = product+product_t[k]
                else:
                    product = product+'_'+product_t[k]
            products.append(product)
            units.append(unit)
        selection['products'] = products
        selection['units'] = units
        columns = ['products','units']
        columns.extend(ghg_emissions)
        selection = selection[columns]
        length = len(selection)
        data_t = [[] for x in range(length)]
        for i in range(length):
            data_t[i] = list(selection.iloc[i,:])
        data = [[] for x in range(1)]
        data[0] = list(selection.columns)
        data.extend(data_t)
        return(data)
    
    io_relations = [['primary product','indirect products','','']]
    io_secondary = []
    selection_indirect = []
    for product in selection['products']:
        temp = ExtractIOInfo(product)
        t_product = temp.columns[0]
        product = [t_product,'','','']
        if len(temp[t_product])>0:
            io_relations.append(product)
            for indirect in list(temp.index):
                t_string = indirect.split('.')
                unit = t_string[-1]
                t_string.remove(unit)
                t_product = "_".join(t_string)
                io_row = ['', t_product, round(float(temp.loc[indirect]), 2), unit]
                io_relations.append(io_row)
        temp_list = list(temp.index.values) 
        for parameter in temp_list:
            temp2 = ExtractIOInfo(parameter)
            temp_list2 = list(temp2.index.values) 
            if len(temp2[parameter])>0 and parameter not in list(selection['products']) and [parameter, "", "", ""] not in io_relations:
                io_secondary.append([parameter,'','',''])
                for indirect in list(temp2.index):
                    if indirect not in temp_list2:
                        temp_list2.append(indirect)
                    t_string = indirect.split('.')
                    unit = t_string[-1]
                    t_string.remove(unit)
                    t_product = "_".join(t_string)
                    io_row = ['', t_product, round(float(temp2.loc[indirect]), 2), unit]
                    io_secondary.append(io_row)                 
            temp_list.extend([x for x in temp_list2 if x not in temp_list])
        selection_indirect.extend(temp_list)

    io_relations.extend(io_secondary)
    
    io_relations_unique = [io_relations[0]]
    for i in range(len(io_relations)):
        if i != 0:
            temprow = io_relations[i]
            tempproduct = temprow[0]
            if tempproduct != "" and temprow not in io_relations[0:i]:
                nextrow = next(x for x in io_relations[i+1:len(io_relations)] if x[1:4] == ['','',''])
                nextindex = io_relations[i+1:len(io_relations)].index(nextrow) +i+1
                io_relations_unique.extend(io_relations[i:nextindex])
                
    io_relations = io_relations_unique
    
    selection_indirect = np.unique(selection_indirect).tolist()
    selection_indirect = (set(selection_indirect) - set(selection['products']))
    selection_indirect = pd.DataFrame(selection_indirect)
    selection_indirect.columns = ['products']
    
    data = ExtractImpacts(selection)
    for i in range(len(data)):
        if i != 0:
            number = random.randint(0,10)
            data[i].append(number)
        else:
            data[0].append('Ref #')
    data_indirect = ExtractImpacts(selection_indirect)
    for i in range(len(data_indirect)):
        if i != 0:
            number = random.randint(0,10)
            data_indirect[i].append(number)
        else:
            data_indirect[0].append('Ref #')
        
    c = canvas.Canvas(file_name, pagesize = letter)
    width, height = letter 

    margin = 1*inch     
    ceiling = height-margin-(16) 
    middle = width/2
  
    c.setFont('Helvetica-Bold',16)
    c.drawCentredString(middle,ceiling,"BioC2G Documentation")
    
    ceiling += -(.3*inch)
    c.setFont('Helvetica-Bold',12)
    c.drawCentredString(middle, ceiling, 'Life Cycle Assessment Methodology')

    message = 'There are four steps involved in a life cycle assessment (LCA) study according to ISO 14040 — the international standard that delineates the methodology to conduct LCAs (Finkbeiner et al. 2006). These steps include goal and scope definition, life cycle inventory development, life cycle impact assessment and result interpretation. <br/><br/><b>Goal and scope definition</b>: A system boundary is defined encompassing all the material and energy flows throughout every stage in the life-cycle of a given system. This LCA tool uses system expansion to account for the system of processes related to the use of co-products or by-products. <br/> <br/><b>Life Cycle Inventory (LCI) development</b>: Material and energy flows for the system are compiled for the system boundary defined in goal and scope definition. For this tool, unit emission rates and emission factors were collected for all life-cycle stages involved (Table 1 and 2). This tool considers life-cycle stages such as feedstock production, transportation, fuel production, co-product use, and waste disposal. <br/> <br/> <b>Life Cycle Impact Assessment (LCIA)</b>: This step includes characterizing and estimating environmental impacts from the set of material/energy flows and impact factors. There are several midpoint and endpoint LCIA methods to aggregate environmental impacts such as TRACI 2.1 (Bare 2011) and eco-indicator 99 (Goedkoop and Spriensma 2000). TRACI is developed by EPA and is the most commonly used LCIA method. TRACI includes ten environmental impacts: global warming potential, eutrophication potential, acidification, two ecotoxicity impacts, three human health impacts, photochemical smog creation potential, and ozone depletion potential. This LCA tool generates illustrative results for CO2<sub>eq</sub> emissions (global warming potential) and water consumption impact from bioenergy production scenarios. <br/> <br/><b>Result Interpretation</b>: The results are quantified as a function of energy produced for easier comparisons of bioenergy types.'
    P = Paragraph(message, style)
    wp,hp = P.wrapOn(c,6.5*inch,270)
    ceiling += -(hp + .2*inch)
    P.drawOn(c, x = margin, y = ceiling)
    
    ceiling += -(.3*inch)
    c.setFont('Helvetica-Bold',12)
    c.drawCentredString(middle, ceiling, 'Model Specifics')
    
    message = '<b>Allocation</b>: System Expansion<br/><br/><b>Model Type</b>: '+model+'<br/><br/><b>Fuel</b>: '+fuel.replace('_',' ').title()+' ('+typefuel.capitalize()+")<br/><br/><b>Feedstock</b>: "+feedstock.capitalize()+"<br/><br/><b>Functional Unit</b>: 1 MJ of "+fuel.replace('_',' ').title()+"<br/><br/><b>Offset Credits</b>: Electricity (NGCC); Steam"
    P = Paragraph(message, style)
    wp,hp = P.wrapOn(c,6.5*inch,270)
    ceiling += -(hp + .2*inch)
    P.drawOn(c, x = margin, y = ceiling)
    
    c.showPage()
    
    ceiling = height-margin -(12)
    c.setFont('Helvetica-Bold',12)
    c.drawCentredString(middle, ceiling, 'Emission Factors')
    
    message1 = 'Table 1 lists the emission factors directly called in the LCA of the user-defined process/product. The units are kg of specified emission type per listed unit of respective product.'
    P1 = Paragraph(message1, style)
    wp1,hp1 = P1.wrapOn(c,6.5*inch,24)
    ceiling += -(hp1 + .2*inch)
    P1.drawOn(c, x = margin, y = ceiling)
    
    ceiling += -.2*inch
    c.setFont('Helvetica-Bold',10)
    c.drawCentredString(middle, ceiling, 'Table 1: Direct Products')
    
    ef_table_style = TableStyle([('INNERGRID', (0,0), (-1,-1), 0.2, colors.black),
                            ('BOX', (0,0), (-1,-1), 0.2, colors.black),
                            ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                            ('SIZE', (0,0), (-1,-1), 10),
                            ('ALIGN',(-1,0),(-1,-1),'CENTER'),
                            ('ALIGN', (0,0),(-1,0),'CENTER')])
    
    t = Table(data, colWidths=([1.85*inch]+[.75*inch]+[1*inch for x in range(3)]+[.5*inch]), rowHeights = [20 for x in range(len(data))])
    t.setStyle(ef_table_style)
    
    wt1,ht1 = t.wrapOn(c, 0, 0)
    ceiling += -(20*(len(data)) + .1*inch)
    t.drawOn(c, x = (width - sum([1.85*inch]+[.75*inch]+[1*inch for x in range(3)]+[.5*inch]))/2, y = ceiling)
    

    message2 = 'Table 2 lists all other emission factors used in the LCA of the user-defined process/product. These factors are indirectly used in the model to account for the upstream or downstream impacts of the products/processes listed above. The units are kg of specified emission type per listed unit of respective product.'
    P2 = Paragraph(message2, style)
    wp2,hp2 = P2.wrapOn(c,6.5*inch,50)
    ceiling += -(hp2 + .2*inch)
    if ceiling < margin:
        c.showPage()
        ceiling = height - margin - hp2
    P2.drawOn(c, x = 1*inch, y = ceiling)

    ceiling += -.2*inch
    ceiling_t = ceiling
    
    t2_drawwidth = (width - sum([1.85*inch]+[.75*inch]+[1*inch for x in range(3)]+[.5*inch]))/2
    t2 = Table(data_indirect, colWidths=([1.85*inch]+[.75*inch]+[1*inch for x in range(3)]+[.5*inch]), rowHeights = [20 for x in range(len(data_indirect))])
    t2.setStyle(ef_table_style)
    wt2,ht2 = t2.wrapOn(c, 0, 0)
    ceiling += -(20*(len(data_indirect)) + .1*inch)
    if ceiling_t >= margin+3*20+10+.1*inch:
        c.setFont('Helvetica-Bold',10)
        c.drawCentredString(middle, ceiling_t, 'Table 2: Indirect Products')
        if ceiling >= margin+3*20:
            t2.drawOn(c, x = t2_drawwidth, y = ceiling)       
        else:
            first_t2 = int((ceiling_t-margin-10-.1*inch)//20)
            t2 = Table(data_indirect[0:first_t2], colWidths=([1.85*inch]+[.75*inch]+[1*inch for x in range(3)]+[.5*inch]), rowHeights = [20 for x in range(len(data_indirect[0:first_t2]))])
            t2.setStyle(ef_table_style)
            wt2,ht2 = t2.wrapOn(c, 0, 0)
            t2.drawOn(c, x = t2_drawwidth, y = margin)       
            c.showPage()
            row_max = 32
            remainder_pages = math.ceil((len(data_indirect) - first_t2 - 1)/(row_max))
            for i in range(remainder_pages):
                cut_start = int(first_t2+i*(row_max-1))
                cut_end = int((first_t2+i*(row_max-1))+(row_max-1))
                if cut_end > len(data_indirect):
                    cut_end = -1
                t2_data = [data_indirect[0]]
                t2_data.extend(data_indirect[cut_start:cut_end])
                t2 = Table(t2_data, colWidths=([1.85*inch]+[.75*inch]+[1*inch for x in range(3)]+[.5*inch]), rowHeights = [20 for x in range(len(t2_data))])
                t2.setStyle(ef_table_style)
                wt2,ht2 = t2.wrapOn(c, 0, 0)    
                if i < remainder_pages-1:
                    t2.drawOn(c, x = t2_drawwidth, y = margin)
                    c.showPage()
                else:    
                    t2.drawOn(c, x = t2_drawwidth, y = height-margin-len(t2_data)*20)
                    ceiling = height-margin-len(t2_data)*20
    else:
        c.showPage()
        ceiling = height - margin - hp2
        c.setFont('Helvetica-Bold',10)
        c.drawCentredString(middle, ceiling, 'Table 2: Indirect Products')
        ceiling += -.1*inch
        t2.drawOn(c, x = (width - sum([1.85*inch]+[.75*inch]+[1*inch for x in range(3)]+[.5*inch]))/2, y = ceiling)       
    
    message3 = 'Table 3 describes the amount of each indirect product/process required for each primary product/process. If any of the emission factors listed in Table 1 or Table 2 do not appear in the "primary product" column of the table below, it does not have any upstream or downstream impacts.'
    P3 = Paragraph(message3, style)
    wp3,hp3 = P3.wrapOn(c,6.5*inch,30)
    ceiling += -(hp3 + .2*inch)
    if ceiling < margin:
        c.showPage()
        ceiling = height - margin - hp3
    P3.drawOn(c, x = 1*inch, y = ceiling)
    
    ceiling_temp = ceiling
    
    t_io_df = pd.DataFrame.from_records(io_relations)
    t_box = []
    for i in range(len(t_io_df)):
        row_1 = t_io_df.loc[i,:][0]
        if (i+1) < len(t_io_df):
            row_2 = t_io_df.loc[i+1,:][0]
            if row_1 == "" and row_2 != "":
                t_x = i+1 
                t_box.append(t_x)
    def TableStyle_BoxIndex(t_box):
        t_box_style = []
        for k in range(len(t_box)):
            index = t_box[k]
            if (k+1)< len(t_box):
                index2 = t_box[k+1]
                temp = ('BOX', (0,index), (-1,(index2-1)), 0.2, colors.black)
                t_box_style.append(temp)
        return(t_box_style)
    
    t3_style = [('BOX', (0,0), (-1,-1), 0.2, colors.black),
             ('BOX', (0,0), (-1,0), 0.2, colors.black),
             ('BOX', (0,0), (0,-1), 0.2, colors.black),
             ('BOX', (2,0), (-1,-1), 0.2, colors.black),
             ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
             ('SIZE', (0,0), (-1,-1), 10),
             ('ALIGN',(2,1),(-2,-1),'RIGHT'),
             ('ALIGN', (0,0),(-1,0),'CENTER')]
    
    t3_style1 = t3_style.copy()
    
    t3_style1.extend(TableStyle_BoxIndex(t_box))    
    t3 = Table(io_relations, colWidths=([1.75*inch]*2+[1*inch]*2), rowHeights = [12 for x in range(len(io_relations))])
    t3.setStyle(TableStyle(t3_style1))
    
    wt3,ht3 = t3.wrapOn(c, 0, 0)
    
    t3_title_length = 10+.2*inch
    t3_length = 12*(len(io_relations))+.1*inch
    ceiling += -(t3_title_length+t3_length)
    
    row_max = int((height-margin*2)/12)
    
    if ceiling >= margin:
        c.setFont('Helvetica-Bold',10)
        c.drawCentredString(middle, ceiling+t3_length, 'Table 3: Input Output Table Relations')
        t3.drawOn(c, x = 1.5*inch, y = ceiling) 
        c.showPage()
    else:        
        space_1 = ceiling_temp - margin 
        t3_first = int((space_1 - (t3_title_length+.1*inch))//12)
        if t3_first > 4:
            t_box1 =  [x for x in t_box if x <= t3_first]
            t3_style1 = t3_style.copy()
            t3_style1.extend(TableStyle_BoxIndex(t_box1))
            t3 = Table(io_relations[0:t3_first], colWidths=([1.75*inch]*2+[1*inch]*2), rowHeights = [12 for x in range(t3_first)])
            t3.setStyle(TableStyle(t3_style1))
            wt3,ht3 = t3.wrapOn(c, 0, 0)    
            c.setFont('Helvetica-Bold',10)
            c.drawCentredString(middle, margin+ 12*t3_first+ .1*inch, 'Table 3: Input Output Table Relations')
            t3.drawOn(c, x = 1.5*inch, y = margin)
            c.showPage()
            t3_pages = math.ceil((len(io_relations) - t3_first - 1)/row_max)
            for i in range(int(t3_pages)):
                t_box1 = [(x-t3_first-i*row_max+i+1) for x in t_box if (x-t3_first-i*row_max+i+1) >= 0 and (x-t3_first-i*row_max+i+1) <= row_max]
                t3_style1 = t3_style.copy()
                t3_style1.extend(TableStyle_BoxIndex(t_box1))
                t3_data = [['primary product','indirect products','','']]
                cut_start = int(t3_first+i*(row_max-1))
                cut_end = int((t3_first+i*(row_max-1))+(row_max-1))
                if cut_end > len(io_relations):
                    cut_end = -1
                t3_data.extend(io_relations[cut_start:cut_end])
                t3 = Table(t3_data, colWidths=([1.75*inch]*2+[1*inch]*2), rowHeights = [12]*len(t3_data))
                t3.setStyle(TableStyle(t3_style1))
                wt3,ht3 = t3.wrapOn(c, 0, 0)    
                if i < t3_pages-1:
                    t3.drawOn(c, x = 1.5*inch, y = margin)
                    c.showPage()
                else:    
                    t3.drawOn(c, x = 1.5*inch, y = height-margin-len(t3_data)*12)
                    c.showPage()
        else:                                                               
            c.showPage()
            ceiling = height-margin-10
            c.setFont('Helvetica-Bold',10)
            c.drawCentredString(middle, ceiling, 'Table 3: Input Output Table Relations')
            t3_pages = math.ceil((len(io_relations) - 1)/(row_max-1))
            cut_start = 1
            for i in range(int(t3_pages)):
                row_max = 54
                if i==0:
                    i_t = 0
                else:
                    i_t = 1
                t_box1 = [(x-i*row_max+i+2*i_t) for x in t_box if (x-i*row_max+i+2*i_t) >= 0 and (x-i*row_max+i+2*i_t) <= row_max]
                #if i == 0:
                #    t_box1 = [(x-i*row_max+i) for x in t_box if (x-i*row_max+i) >= 0 and (x-i*row_max+i) <= row_max]
                #else:
                #    t_box1 = [(x-i*row_max+i-1+1(i-1)) for x in t_box if (x-i*row_max+i-1+1(i-1)) >= 0 and (x-i*row_max+i-1+1(i-1)) <= row_max]
                #t_box1 = [(x-1-i*row_max) for x in t_box if (x-1-i*row_max) >= 0 and (x-1-i*row_max) < row_max]
                t3_style1 = t3_style.copy()
                t3_style1.extend(TableStyle_BoxIndex(t_box1))
                t3_data = [['primary product','indirect products','','']]                
                if i == 0:
                    row_max = 51
                if i == 1:
                    cut_start += 51
                    row_max = 53
                if i > 1:
                    cut_start += 53
                    row_max = 53
                cut_end = cut_start + row_max
                if cut_end > len(io_relations):
                    cut_end = -1
                t3_data.extend(io_relations[cut_start:cut_end])
                t3 = Table(t3_data, colWidths=([1.75*inch]*2+[1*inch]*2), rowHeights = [12]*len(t3_data))
                t3.setStyle(TableStyle(t3_style1))
                wt3,ht3 = t3.wrapOn(c, 0, 0)    
                if i < t3_pages-1:
                    t3.drawOn(c, x = 1.5*inch, y = margin)
                    c.showPage()
                else:    
                    t3.drawOn(c, x = 1.5*inch, y = height-margin-len(t3_data)*12)
                    c.showPage()
    ceiling = height-margin -(12)
    c.setFont('Helvetica-Bold',12)
    c.drawCentredString(middle, ceiling, 'References')
    style = ParagraphStyle(name= 'Custom',
                         fontName='Helvetica',
                         fontSize=10,
                         leftIndent=40,
                         firstLineIndent=-40,
                         parent = styleSheet['BodyText'])
    ref1 = "1. EPA. (1993). Synthetic Ammonia, Chapter 8: Inorganic Chemical Industry. In AP-42: Compilation of Air Emissions Factors. U.S. Environmental Protection Agency."
    ref2 = "2. EPA. (2013). Inventory of U.S. Greenhouse Gas Emissions and Sinks: 1990 - 2011 (No. 430-R-13001). U.S. Environmental Protection Agency."
    ref3 = "3. Hanle, L., Maldonado, P., Onuma, E., Tichy, M., & van Oss, H. G. (2006). Chapter 2: Mineral Industry Emissions. In 2006 IPCC Guidelines for National Greenhouse Gas Inventories (Vol. 3). IPCC."
    ref4= "4. Lee, U., Han, J., & Wang, M. (2017). Evaluation of landfill gas emissions from municipal solid waste landfills for the life-cycle analysis of waste-to-energy pathways. Journal of Cleaner Production, 166, 335–342. doi:10.1016/j.jclepro.2017.08.016"
    ref5= "5. Office of Air and Radiation. (2009). Technical Support Document for the Ammonia Production Sector: Proposed Rule for Mandatory Reporting of Greenhouse Gases. U.S. Environmental Protection Agency."
    ref6= "6. Research Triangle Institute. (2000). Lime Production: Industry Profile (No. EPA Contract 68-D-99-024). U.S. Environmental Protection Agency."
    ref7= "7. Sheehan, J., Camobreco, V., Duffield, J., Shapouri, H., Graboski, M., & Tyson, K. S. (2000). An overview of biodiesel and petroleum diesel life cycles. Golden, CO (United States): National Renewable Energy Laboratory (NREL). doi:10.2172/771560"
    ref8= "8. Spath, P., & Mann, M. (2001). Life Cycle Assessment of Hydrogen Production via Natural Gas Steam Reforming (No. NREL/TP-570-27637). National Renewable Energy Laboratory."
    ref9= "9. Wang, M. (2008). The Greenhouse Gases, Regulated Emissions, and Energy Use in Transportation (GREET) Model Version 1.5. Center for Transportation Research, Argonne National Laboratory."
    ref10 = "10. Wicke, B., Dornburg, V., Junginger, M., & Faaij, A. (2008). Different palm oil production systems for energy purposes and their greenhouse gas implications. Biomass and Bioenergy, 32(12), 1322–1337. doi:10.1016/j.biombioe.2008.04.001"
    ref_list = [ref1, ref2, ref3, ref4, ref5, ref6, ref7, ref8, ref9, ref10]
    for ref in ref_list:
        P = Paragraph(ref, style)
        wp,hp = P.wrapOn(c,6.5*inch,40)
        ceiling += -(hp + .2*inch)
        P.drawOn(c, x = margin, y = ceiling)

    c.save()                  
            
            

def CompletePDF(file_name):
    
    ## creates PDF file which lists all GHG emission factors saved for LCA model    
     ## ARGs:
     #  file_name = name that the PDF file should save with
    
    filepath_list = [pp.co2_filepath, pp.ch4_filepath, pp.n2o_filepath]
    ghg_emissions = ['CO2','CH4','N2O']
    
    c = canvas.Canvas(file_name, pagesize = letter)
    width, height = letter 
    
    for filepath, emission in zip(filepath_list, ghg_emissions):
        ex = pd.read_csv(filepath)
        #ex = ex[(ex['r'] != 0)]
        length = len(ex['r'])
        data = [[] for x in range(length)]
        for i in range(length):
            data[i] = list(ex.iloc[i,:])
        
        
        c.setFont('Helvetica',12)
        c.drawString(8*cm, 10.55*inch, "LCA Data Assumptions")
        c.setFont('Helvetica',9)
        c.drawString(8.5*cm, 10.425*inch, '%s Emission Factors' % emission)
        
        styleSheet = getSampleStyleSheet()
        style = ParagraphStyle(name= 'Custom',
                                  fontName='Helvetica',
                                  fontSize=7,
                                  parent = styleSheet['BodyText'])
        message = 'The following table is a complete list of the %s emission factors used in the LCA model. The units are kg of %s per listed unit of respective product.'
        P = Paragraph(message % (emission, emission), style)
        w,h = P.wrapOn(c,400,200)
        P.drawOn(c, x = 1*inch, y = 10*inch )
        
        t1 = Table(data[0:round(length/2)], colWidths=[100,75], rowHeights = [10 for x in range(round(length/2))])
        t1.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.2, colors.black),
                               ('BOX', (0,0), (-1,-1), 0.2, colors.black),
                               ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                               ('SIZE', (0,0), (-1,-1), 5)]))
            
        w1,h1 = t1.wrapOn(c, 0, 0)
        t1.drawOn(c, x = 1*inch, y = 12)    
        
        t2 = Table(data[round(length/2):length], colWidths=[100,75], rowHeights = [10 for x in range(length-round(length/2))])
        t2.setStyle(TableStyle([('INNERGRID', (0,0), (-1,-1), 0.2, colors.black),
                               ('BOX', (0,0), (-1,-1), 0.2, colors.black),
                               ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                               ('SIZE', (0,0), (-1,-1), 5)]))
            
        w2,h2 = t2.wrapOn(c, 0, 0)
        t2.drawOn(c, x = 4.25*inch, y = 12)    
        c.showPage() 
    
    c.save()