#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 12:43:02 2019

@author: sarahnordahl
"""
import pandas as pd
import numpy as np

co2_filepath = "waste/io_tables/impact_vectors/co2_impact.csv"
ch4_filepath = "waste/io_tables/impact_vectors/ch4_impact.csv"
n2o_filepath = "waste/io_tables/impact_vectors/n2o_impact.csv"

co_filepath = "waste/io_tables/impact_vectors/co_impact.csv"
nox_filepath = "waste/io_tables/impact_vectors/nox_impact.csv"
pm25_filepath = "waste/io_tables/impact_vectors/pm25_impact.csv"
so2_filepath = "waste/io_tables/impact_vectors/so2_impact.csv"
voc_filepath = "waste/io_tables/impact_vectors/voc_impact.csv"
water_c_filepath = "waste/io_tables/impact_vectors/water_c_impact.csv"
water_w_filepath = "waste/io_tables/impact_vectors/water_w_impact.csv"


def GHGImpactVectorSum(time_horizon):
    # Computes an impact vector equal to the total kg co2 equivalents per physical 
    # output unit for each sector
    #
    # Args:
    #  co2.filepath: csv file path for co2 impact vector (kg/physical output unit)
    #  ch4.filepath: csv file path for ch4 impact vector (kg/physical output unit)
    #  n2o.filepath: csv file path for n2o impact vector (kg/physical output unit)
    #  time.horizon: number of years used for time horizon of IPCC factors - 
    #   default is 100 years, can also to 20
    # Returns:
    #  The total kg co2e for each sector in a vector form
    filepaths = [co2_filepath, ch4_filepath, n2o_filepath]
    # IPCC 100-year multipliers for different GHGs to normalize to CO2e
    ipcc_values = {'ipcc_ch4_100': 28,  
                 'ipcc_ch4_20': 72, 
                 'ipcc_n2o_100': 298,  
                 'ipcc_n2o_20': 289}
    ipcc_multipliers = [1, ipcc_values["ipcc_ch4_{}".format(time_horizon)], ipcc_values["ipcc_n2o_{}".format(time_horizon)]]

    ghg_total_kg = 0

    for x in range(3):
        impact = pd.read_csv(filepaths[x]).loc[:,'r']
        impact *= ipcc_multipliers[x]
        ghg_total_kg = ghg_total_kg + impact

    return ghg_total_kg


def IOSolutionPhysicalUnits(A, y):
    # Solves for total requirements from each sector in in physical units
    #
    # Args:
    #  A: input-output vector in physical units
    #  y: direct requirements vector
    # 
    # Returns:
    #  The total (direct + indirect) requirements by sector
    #num_sectors = A.shape[1] 
    I = np.eye(A.shape[1])
    solution = np.linalg.solve((I - A), y)
    return solution

def TotalGHGEmissions(io_data, y, time_horizon):
    # Returns a vector of of all GHG emissions in the form of kg CO2e
    #
    # Args:
    #  A: input-output vector in physical units ratios
    #  y: direct requirements vector in physical units
    #  co2.filepath: filepath to csv file containing kg CO2/kg output for 
    #   each sector
    #  ch4.filepath: filepath to csv file containing kg CH4/kg output for 
    #   each sector
    #  n2o.filepath: filepath to csv file containing kg N2O/kg output for 
    #   each sector
    #  time.horizon: number of years used for time horizon of normalized GHG 
    #   forcing
    #  biorefinery.direct.ghg: kg fossil CO2e emitted directly at the biorefinery
    #  combustion.direct.ghg: kg fossil CO2e emitted during product combustion/
    #   end-of-life.  Only applicable where some fossil carbon is in product
    #   or there is net biogenic carbon sequestration
    # Returns:
    #  The net GHG emissions (kg CO2e) for the product life cycle by sector

    A = io_data.drop(['products'],1).values.T
    y_array = []
    for item in io_data['products']:
        if item in y:
            temp = y[item]
        else:
            temp = 0
        y_array.append(temp)

    io_ghg_results_kg = IOSolutionPhysicalUnits(A, y_array) * GHGImpactVectorSum(time_horizon)
    #io_ghg_results_kg = np.append(io_ghg_results_kg,[biorefinery_direct_ghg, cooled_water_ghg, steam_ghg, catalysts])
    rownames = io_data['products']
    io_ghg_results_kg_df = pd.DataFrame(io_ghg_results_kg)
    io_ghg_results_kg_df.columns = ["ghg_results_kg"]
    io_ghg_results_kg_df['products'] = rownames
    return io_ghg_results_kg_df

def TotalEmissions(io_data, y, emission_type, time_horizon):
    # Returns a vector of emissions of specified type in kg
    #
    # Args:
    #  io_data: input-output vector in physical units ratios
    #  y: direct requirements vector in physical units
    #  emission_type: options: co, nox, pm25, so2, voc, water_c, water_w
    #  time.horizon: number of years used for time horizon of normalized GHG 
    #   forcing
    # Returns:
    #  The net emissions (kg) for the product life cycle by sector

    A = io_data.drop(['products'],1).values.T
    y_array = []
    for item in io_data['products']:
        if item in y:
            temp = y[item]
        else:
            temp = 0
        y_array.append(temp)
        
    guide_dict = {'co': co_filepath,
                  'nox': nox_filepath,
                  'pm25': pm25_filepath,
                  'so2': so2_filepath,
                  'voc': voc_filepath,
                  'water_c': water_c_filepath,
                  'water_w': water_w_filepath}    
    
    emission_vector = pd.read_csv(guide_dict[emission_type]).loc[:,'r']
    io_results_kg = IOSolutionPhysicalUnits(A, y_array) * emission_vector
    #io_ghg_results_kg = np.append(io_ghg_results_kg,[biorefinery_direct_ghg, cooled_water_ghg, steam_ghg, catalysts])
    rownames = io_data['products']
    io_results_kg_df = pd.DataFrame(io_results_kg)
    io_results_kg_df.columns = [(emission_type+"_results_kg")]
    io_results_kg_df['products'] = rownames
    return io_results_kg_df

def AggregateResults(m, results_kg_co2e, scenario, year):
    
    ## ARG:
    ## results_kg_co2e must be a dictionary 
    
    ## double check later to make sure every "product" in y is reflected somewhere in m 
    
    m[scenario].loc["Petroleum Products"] = sum([results_kg_co2e["rfo.MJ"], 
                                                  results_kg_co2e["refgas.MJ"],
                                                  results_kg_co2e["gasoline.MJ"],
                                                  results_kg_co2e["crudeoil.MJ"]])
    m[scenario].loc["Diesel"] = sum([results_kg_co2e["diesel.MJ"], 
                                                  results_kg_co2e["diesel_combust.MJ"]])
    m[scenario].loc["Other Electricity"] =  sum([results_kg_co2e["electricity.US.kWh"], 
                                                  results_kg_co2e["electricity.NG.kWh"],
                                                  results_kg_co2e["electricity.Coal.kWh"],
                                                  results_kg_co2e["electricity.Lignin.kWh"],
                                                  results_kg_co2e["electricity.Renewables.kWh"],
                                                  results_kg_co2e["electricity.WECC.kWh"],
                                                  results_kg_co2e["electricity.MRO.kWh"],
                                                  results_kg_co2e["electricity.SPP.kWh"],
                                                  results_kg_co2e["electricity.TRE.kWh"],
                                                  results_kg_co2e["electricity.SERC.kWh"],
                                                  results_kg_co2e["electricity.RFC.kWh"],
                                                  results_kg_co2e["electricity.NPCC.kWh"],
                                                  results_kg_co2e["electricity.FRCC.kWh"]])
    if(year == 2020):
         m[scenario].loc["NGCC Electricity"] = results_kg_co2e["electricity.NGCC.kWh"]
    else:
         m[scenario].loc["NGCC Electricity"] = sum([results_kg_co2e["electricity.NGCC.kWh"],
                                                   results_kg_co2e["Electricity_GridMix_2050.kWh"]])
    m[scenario].loc["Natural Gas"] = sum([results_kg_co2e["naturalgas.MJ"], 
                                                  results_kg_co2e["naturalgas_combust.MJ"]])
    m[scenario].loc["Transportation"] = sum([results_kg_co2e["flatbedtruck.mt_km"], 
                                                  results_kg_co2e["tankertruck.mt_km"],
                                                  results_kg_co2e["gaspipeline.mt_km"],
                                                  results_kg_co2e["liquidpipeline.mt_km"],
                                                  results_kg_co2e["rail.mt_km"],
                                                  results_kg_co2e["marinetanker.mt_km"],
                                                  results_kg_co2e["barge.mt_km"]])
    m[scenario].loc["Landfill"] =  sum([results_kg_co2e["landfill_cardboard.wet_kg"], 
                                                  results_kg_co2e["landfill_magazines.wet_kg"],
                                                  results_kg_co2e["landfill_newspaper.wet_kg"],
                                                  results_kg_co2e["landfill_officepaper.wet_kg"],
                                                  results_kg_co2e["landfill_phonebooks.wet_kg"],
                                                  results_kg_co2e["landfill_textbooks.wet_kg"],
                                                  results_kg_co2e["landfill_lumber.wet_kg"],
                                                  results_kg_co2e["landfill_fiberboard.wet_kg"],
                                                  results_kg_co2e["landfill_foodwaste.wet_kg"],
                                                  results_kg_co2e["landfill_foodwaste_nonmeat.wet_kg"],
                                                  results_kg_co2e["landfill_foodwaste_meat.wet_kg"],
                                                  results_kg_co2e["landfill_beef.wet_kg"],
                                                  results_kg_co2e["landfill_poultry.wet_kg"],
                                                  results_kg_co2e["landfill_grains.wet_kg"],
                                                  results_kg_co2e["landfill_fruit_vegetables.wet_kg"],
                                                  results_kg_co2e["landfill_dairy_products.wet_kg"],
                                                  results_kg_co2e["landfill_yard_trimmings.wet_kg"],
                                                  results_kg_co2e["landfill_grass.wet_kg"],
                                                  results_kg_co2e["landfill_leaves.wet_kg"],
                                                  results_kg_co2e["landfill_branches.wet_kg"],
                                                  results_kg_co2e["landfill_mixedpaper.wet_kg"],
                                                  results_kg_co2e["landfill_residentialpaper.wet_kg"],
                                                  results_kg_co2e["landfill_mixedofficepaper.wet_kg"],
                                                  results_kg_co2e["landfill_mixedrecyclables.wet_kg"],
                                                  results_kg_co2e["landfill_mixedorganics.wet_kg"],
                                                  results_kg_co2e["landfill_mixedMSW.wet_kg"],
                                                  results_kg_co2e["landfill_inorganics_wet.kg"]])
    m[scenario].loc["Compost Application"] = results_kg_co2e["compost_application.kg"]
    m[scenario].loc["Sequestration"] = results_kg_co2e["landfill_CO2_sequestration.kg"]
    m[scenario].loc["Biochar Application"] = results_kg_co2e["biochar_ag_application.kg"]
    m[scenario].loc['Chemicals'] = sum([results_kg_co2e["h2so4.kg"], 
                                                  results_kg_co2e["naoh.kg"],
                                                  results_kg_co2e["ferric_chloride.kg"],
                                                  results_kg_co2e["polymers_WWT.kg"],
                                                  results_kg_co2e["calcium_hypochlorite.kg"]])
    m[scenario].loc["Organics Composting"] = sum([results_kg_co2e["organics_composting_wet.kg"], 
                                                  results_kg_co2e["outdoor_compost.wet_kg"]])
    m[scenario].loc["Digestate Application"] = results_kg_co2e["biosolids_land_application_dry.kg"]
    m[scenario].loc["Fertilizer Use"] = sum([results_kg_co2e["ammonia.kg"],results_kg_co2e["urea.kg"]])
    m[scenario].loc["CHP"] = results_kg_co2e["biogas_CHP.m3"]
    m[scenario].loc["Facility Flare"] = results_kg_co2e["biogas_flare.m3"]
    m[scenario].loc["Biofilter Release"] = results_kg_co2e["biogas_biofilter.m3"]
    m[scenario].loc["Methane Loss"] = results_kg_co2e["CNG Loss"]
    m[scenario].loc['Other'] = sum(results_kg_co2e.values()) - sum(m[scenario])

    















